getBins <- function(x, binwidth, ...){

  args.list <- list(...)
  output <- list()
  step <- 1/10^(getDecimals(binwidth)+1)

  if("levels" %in% names(args.list)){
    output$levels.v <- args.list$levels
    }else{
    output$levels.v <- sort(binwidth* unique(x  %/% binwidth))
    }

  if("labels" %in% names(args.list)){
    output$labels.v <- args.list$labels
    }else{
    output$labels.v <- paste(output$levels.v, output$levels.v+(binwidth-step), sep=" : ")
    }

  #output$labels.v <- labels.v <- paste(levels.v, levels.v+(binwidth-step), sep=" : ")
  x.bin <- binwidth *(x  %/% binwidth)
  output$x.bin.f <- factor(x.bin, levels = output$levels.v, labels = output$labels.v )
  output$x.bin <- x.bin
  output

}#getbins




plotCPUEVsTide <- function(data.set.tides, year.emph=NA, emph.date=NA, df=3, col='black', filename=NA){
 require(ggplot2)
  #data.set.tides$cpue <- log(data.set.tides$cpue+1)
 fit <- loess(cpue~rate.avg, data = data.set.tides)
 #fit <- glm(cpue~splines::ns(rate.avg,df), data = data.set.tides, family="quasipoisson")
 predict.df <- data.frame(rate.avg=sort(unique(data.set.tides$rate.avg)))
 predict.df$fitted <- predict(fit, newdata = predict.df)

 #library(splines)
 #qplot(x, y, data=sites) + stat_smooth(method="glm", family="quasipoisson", formula = y ~ ns(x, 3))

 p <- ggplot(data.set.tides, aes(rate.avg, cpue))+
  theme_bw()+
  geom_point(shape=1, size=0.65, alpha=0.5, col='darkgrey')+
  {if(any(!is.na(year.emph)) & is.na(emph.date)) {
    geom_point(data=data.set.tides[data.set.tides$year %in% year.emph ,],shape=16, size=1.5, col=col)
  }else if(any(!is.na(year.emph)) & !is.na(emph.date)){
    data.set.tides$md <- format(data.set.tides$fe_start_date, "%m-%d")
#    geom_point(data=data.set.tides[data.set.tides$year %in% year.emph ,],shape=16, size=1.5, col=col)
    geom_text(data=data.set.tides[data.set.tides$year %in% year.emph ,], aes(label=md),size=emph.date, col=col)
  }}+
  geom_line(data=predict.df, aes(rate.avg, fitted), col='red', lwd=1)+
  xlab("Tidal Height Change Rate (meters / 15mins)")+ylab("CPUE (#fish / hour)")

 if(! is.na(filename)){
  #filename <- "cpueVstidechange.png"
  ggsave(filename, p, width = 11, height = 6, dpi=600)
  }
 return(p)

}#END plotCPUEVsTide




plotCumulative <- function(data.cpue, data.predict=NA, data.quants=NA, step.vec, filename=NA){

  for(step in step.vec){
    p <- ggplot(NULL)+
      geom_point(data=data.cpue, aes(date.standard, prop.cumul), size=0.5)+
      scale_x_date(date_breaks = "10 day",date_minor_breaks = "2 day", date_labels = "%b-%d", guide = guide_axis(angle = 90), name = "Date in 2021")+
      ylab("Cumulative Probability of Run")
    if(step>=2) p <- p + geom_line(data=data.predict, aes(date.standard, mean), size=2)
    if(step>=3) p <- p + geom_ribbon(data = data.predict, aes(x=date.standard, ymin=lowerCI, ymax=upperCI), alpha = 0.2 , fill='blue' )
    if(step>=4) p <- p + geom_line(data=data.quants, aes(x=date.standard, y=q, group=probs.f))
    if(step>=5) p <- p + geom_line(data=data.quants, aes(x=date.standard, y=q.logit, group=probs.f), col='red')
    #geom_point(data=data.quants.prop[data.quants.prop$probs %in% c(.025, .975),], aes(x=date.standard, y=p, group=probs.f),shape=1, col='blue')+
    if(step>=6) p <- p + geom_errorbarh(data=data.quants.prop.wide, aes(y=p, xmin=date.standard.0.025, xmax=date.standard.0.975), col='blue')
    #geom_errorbar(data=data.daily.stats, aes(x=date.standard.f, ymin=lowerCI, ymax=upperCI), width=.2, col='red')+

    if(! is.na(filename)){
    filename <- paste0("../../plots/CumulativeProbabilityofRun_outliersexcluded",step, ".png")
    ggsave(filename = filename,plot = p, height = 8, width = 16, dpi = 600)
    }
  }
  return(p)
}#END plotcumulative





plotPercentileDates <- function(data.percentile.date, filename=NA){

  p <- ggplot(data=data.percentile.date, aes(year, date.standard))+
    #geom_vline(xintercept = c(1987.5, 2007.5), col='grey')+
    geom_point()+
    geom_line()+
    xlab("Year")+ylab("Standard Date")+
    #scale_y_date(date_breaks = "6 days", date_labels = "%b-%d")+
    scale_x_continuous( breaks = seq(min(data.percentile.date$year), max(data.percentile.date$year), by=4), guide = guide_axis(angle = 45))+
    facet_grid(vars(p.f), scales = "free_y")

  if(! is.na(filename)){
    #filename <- "../../plots/percentile_dates_timeseries_outliersexcluded.png"
    ggsave(filename,plot = p, height = 11, width=6,dpi=400)
  }else{
   print(p)
  }

}#END plotPercentileDates


plotYearcountVsDay <- function(data.set, filename=NA){

  data.yearcount.byday <- unique(data.set[,c("year", "date.standard")])
  data.yearcount.byday <- aggregate(cbind(year.count=year)~date.standard, data=data.yearcount.byday, length)

  p <- ggplot(data=data.yearcount.byday, aes(date.standard, year.count))+
    geom_point()+
    geom_line()+
    scale_x_date(breaks = "4 days",date_labels = "%b-%d", guide = guide_axis(angle = 90))+
    ylab("Number of Years")+xlab("Standard Date")
  print(p)

  if(! is.na(filename)){
  #filename <- paste0("../../plots/effort(years)_vs_day_scatterplot", ".png")
  ggsave(filename = filename,plot = p, height = 8, width = 16, dpi = 600)
  }

}#END plotYearcountVsDay




plotSetsVsDay <- function(data.set, filename=NA){

  data.setcount.byday <- data.frame(date.standard=unique(data.set$date.standard), setcount=c(table(data.set$date.standard)))

  p <- ggplot(data=data.setcount.byday, aes(date.standard, setcount))+
    geom_vline(xintercept = as.Date("2021-06-10"), col='grey')+
    geom_point()+
    geom_line()+
    scale_x_date(breaks = "4 days",date_labels = "%b-%d", guide = guide_axis(angle = 90))+
    ylab("Number of Sets")
  print(p)

  if(! is.na(filename)){
  #filename <- paste0("../../plots/sets_by_day_scatterplot", ".png")
  ggsave(filename = filename,plot = p, height = 8, width = 16, dpi = 600)
  }

}#END plotSetsVsDay




plotSetsVsYear <- function(data.set, filename=NA){

  data.setcount.byyear <- aggregate(cbind(setcount=fe_start_date)~year, data=data.set, length)

  p <- ggplot(data=data.setcount.byyear, aes(year, setcount))+
    geom_point()+
    geom_line()+
    scale_x_continuous( breaks = seq(min(data.setcount.byyear$year), max(data.setcount.byyear$year), by=4), guide = guide_axis(angle = 90))+
    ylab("Number of Sets")+
    ggtitle("Annual count of sets starting June 10")
  print(p)

  if(! is.na(filename)){
   #filename <- paste0("../../plots/sets_by_year_scatterplot", ".png")
   ggsave(filename = filename,plot = p, height = 8, width = 16, dpi = 600)
  }

}#END plotSetsVsYear







plotSetsVsYearLatticeDay <- function(data.set, filename=NA){

  data.set$md <- factor(format(data.set$date.standard, "%b-%d"), levels=unique(format(data.set$date.standard, "%b-%d"))[order(unique(data.set$date.standard))])
  data.setcount.byyear.byday <- aggregate(cbind(setcount=fe_start_date)~year+date.standard+md, data=data.set, length)

  p <- ggplot(data=data.setcount.byyear.byday, aes(year, setcount))+
    ylim(1,4)+
    geom_point(shape=1, alpha=0.75, size=0.1)+
    geom_smooth(method='loess', se=FALSE)+
    #scale_x_continuous(breaks = seq(0,125, by=5), guide = guide_axis(angle = 90))+
    scale_x_continuous( breaks = seq(min(data.setcount.byyear$year), max(data.setcount.byyear$year), by=4), guide = guide_axis(angle = 90))+
    ylab("Number of Sets")+xlab("Year")+
    facet_wrap(vars(md))
  print(p)

  if(! is.na(filename)){
  #filename <- paste0("../../plots/sets_vs_year_facet_day_scatterplot", ".png")
  ggsave(filename = filename,plot = p, height = 16, width = 8, dpi = 600)
  }

}#END plotSetsVsYearLatticeDay



plotDayCountVsYear <- function(data.set, filename=NA){

  data.daycount.byyear <- unique(data.set[,c("year", "date.standard")])
  data.daycount.byyear <- aggregate(cbind(day.count=date.standard)~year, data=data.daycount.byyear, length)

  p <- ggplot(data=data.daycount.byyear, aes(year, day.count))+
    geom_point()+
    geom_line()+
    #geom_smooth(method='loess', se=FALSE)+
    scale_x_continuous( breaks = seq(min(data.setcount.byyear$year), max(data.setcount.byyear$year), by=4), guide = guide_axis(angle = 90))+
    ylab("Number of Days")+xlab("Year")
  print(p)
  if(! is.na(filename)){
    #filename <- paste0("../../plots/effort(days)_vs_year_scatterplot", ".png")
    ggsave(filename = filename,plot = p, height = 8, width = 16, dpi = 600)
  }

}#END plotDayCountVsYear



plotSetsDailyMeanVsYear <- function(data.set, filename=NA){

  data.set$md <- factor(format(data.set$date.standard, "%b-%d"), levels=unique(format(data.set$date.standard, "%b-%d"))[order(unique(data.set$date.standard))])
  data.setcount.byyear.byday <- aggregate(cbind(setcount=fe_start_date)~year+date.standard+md, data=data.set, length)
  data.setcount.average.byyear <- aggregate(setcount~year, data=data.setcount.byyear.byday, function(x) c(mean=mean(x), sd=sd(x)))

  p <- ggplot(data=data.setcount.average.byyear, aes(year, setcount[,1]))+
    geom_point()+
    geom_line()+
    geom_errorbar(aes(ymin=setcount[,1] - setcount[,2], ymax=setcount[,1] + setcount[,2]), width=0.5)+
    scale_x_continuous( breaks = seq(min(data.setcount.byyear$year), max(data.setcount.byyear$year), by=4), guide = guide_axis(angle = 90))+
    ylab("Number of Sets")+
    ggtitle("Average of daily set count starting June 10")
  print(p)
  if(! is.na(filename)){
  #filename <- paste0("../../plots/sets_dailymean_by_year_scatterplot", ".png")
  ggsave(filename = filename,plot = p, height = 8, width = 16, dpi = 600)
  }
}#END plotSetsDailyMeanVsYear




plotSetTimeMeanVsYear <- function(data.set, filename=NA){

 data.setlength.average.byyear <- aggregate(effort.mins~year, data=data.set, function(x) c(mean=mean(x), sd=sd(x)))

  p <- ggplot(data=data.setlength.average.byyear, aes(year, effort.mins[,1]))+
    ylab("Average Set time (mins)")+
    geom_point()+
    geom_line()+
    geom_errorbar(aes(ymin=effort.mins[,1] - effort.mins[,2], ymax=effort.mins[,1] + effort.mins[,2]), width=0.5)+
    scale_x_continuous( breaks = seq(min(data.setcount.byyear$year), max(data.setcount.byyear$year), by=4), guide = guide_axis(angle = 90))+
   ggtitle("Average Set Time Starting June 10")
  print(p)
  if(! is.na(filename)){
  #filename <- paste0("../../plots/setlength_avg_by_year_scatterplot", ".png")
  ggsave(filename = filename,plot = p, height = 8, width = 16, dpi = 600)
  }

}#END plotSetTimeMeanVsYear




plotSetTimeVsDay_boxplot <- function(data.set, filename=NA){

  p <- ggplot(data=data.set, aes(date.standard, effort.mins))+
    geom_boxplot(aes(group=date.standard), outlier.shape = 1, outlier.size = 0.75)+
    scale_x_date(breaks = "4 days",date_labels = "%b-%d", guide = guide_axis(angle = 90))+
    #scale_y_continuous(breaks = seq(0,1, by=0.1))+
    ylab("effort (mins)")

  p <- p+geom_hline(yintercept = c(50, 109), col='red')
  print(p)

  if(! is.na(filename)){
  #filename <- paste0("../../plots/effort_by_set_boxplot_day_withcutoff", ".png")
  ggsave(filename = filename,plot = p, height = 8, width = 16, dpi = 600)
  }

}#END plotSetTimeVsDay_boxplot




plotCPUEVsSetTime <- function(data.set, filename=NA){

  p <- ggplot(data=data.set, aes(effort.mins, cpue))+
    geom_point(shape=1, alpha=0.75)+
    scale_x_continuous(breaks = seq(0,125, by=5), guide = guide_axis(angle = 90))+
    ylab("cpue (# fish/hour)")+xlab("effort (mins)")
  p <-  ggExtra::ggMarginal(p, type="histogram", margins = "both", binwidth=1)
  print(p)
  if(! is.na(filename)){
  #filename <- paste0("../../plots/cpue_vs_effort_scatterplot", ".png")
  ggsave(filename = filename,plot = p, height = 8, width = 8, dpi = 600)
  }

}#END plotCPUEVsSetTime




plotCPUEVsSetTime_boxplot <- function(data.set, filename=NA, timegroup=5, rectangle=list(xmin=9.5, xmax=21.5, ymin=-1, ymax=39)){

  levels.v <- sort(timegroup* unique(data.set$effort.mins %/% timegroup))
  levels.v <- paste(levels.v, levels.v+(timegroup-1), sep="-")
  data.set$effort.mins.group <- timegroup*data.set$effort.mins %/% timegroup
  data.set$effort.mins.group <- paste(data.set$effort.mins.group, data.set$effort.mins.group+(timegroup-1), sep="-")
  data.set$effort.mins.group <- factor(data.set$effort.mins.group, levels = levels.v )


  p <- ggplot(data=data.set, aes(effort.mins.group, cpue))+
    geom_boxplot(notch = TRUE)+
    ylim(-3,40)+
    theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust=1))+
    #scale_x_continuous(guide = guide_axis(angle = 90))+
    ylab("cpue (# fish/hour)")+xlab("effort (mins)")

  if(! is.null(rectangle)) p <- p+geom_rect( mapping=aes(xmin=rectangle$xmin, xmax=rectangle$xmax, ymin=rectangle$ymin, ymax=rectangle$ymax), fill=NA, col='red')

  print(p)
  if(! is.na(filename)){ ggsave(filename = filename,plot = p, height = 8, width = 12, dpi = 600)}
  #filename <- paste0("../../plots/cpue_vs_settime_group_boxplot_withrectangle", ".png")

}#END plotCPUEVsSetTime_boxplot



plotBoxplot <- function(x,y, binwidth=0.05, xlab=NA, ylab=NA, filename=NA, width=6, height=6,...){

  df <- data.frame(x,y)

  p <- ggplot(df, aes(x, y))+
    geom_boxplot(notch = TRUE)+
    theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust=1))+
    scale_x_discrete( drop=FALSE)

  if(!is.na(xlab)) p <- p+xlab(xlab)
  if(!is.na(ylab)) p <- p+ylab(ylab)

  print(p)
  if(! is.na(filename)){ ggsave(filename, p, width = width, height = height, dpi=600)}

}#END plotBoxplot





plotSetTimeVsYear <- function(data.set, filename=NA){

  p <- ggplot(data=data.set, aes(year, effort.mins))+
    geom_hline(yintercept = c(50,109), col='grey')+
    geom_point(shape=1, alpha=0.25, size=0.75)+
    geom_point(data=data.set[data.set$effort.mins<50 |data.set$effort.mins>109,], shape=1, alpha=0.25, size=0.75, col='red')+
    #scale_x_continuous(breaks = seq(0,125, by=5), guide = guide_axis(angle = 90))+
    xlab("year")+ylab("effort (mins)")
  p <-  ggExtra::ggMarginal(p, type="histogram", margins = "y", binwidth=5)
  print(p)

  if(! is.na(filename)){
  #filename <- paste0("../../plots/effort_vs_year_outliers_scatterplot", ".png")
  ggsave(filename = filename,plot = p, height = 4, width = 8, dpi = 600)
  }

}#END plotSetTimeVsYear


plotCPUEVsDay_facetYear <- function(data.cpue, filename=NA){

  p <- ggplot(data=data.cpue, aes(date.standard, y))+
    geom_vline(xintercept = as.Date(paste0(unique(format(data.cpue$date.standard, "%Y")), "-06-10")), col='red')+
    geom_point(size=0.1)+
    geom_path()+
    ylab("CPUE")+xlab("Standard Date")+
    #scale_x_date(date_breaks = "7 days", date_labels = "%b-%d")+
    scale_x_date(date_breaks = "7 day", date_labels = "%b-%d", guide = guide_axis(angle = 90), name = "Date")+
    facet_wrap(vars(year), ncol = 4)
  print(p)

  if(! is.na(filename)){
    #filename <- "../../plots/cpue_by_year_timeseries.png"
    ggsave(filename, p, width = 20, height = 20, units = "in", dpi = 800)
  }

}#END plotCPUEVsDay_facetYear


plotCPUEVsYear <- function(data.set, filename=NA){

  p <- ggplot(data=data.set, aes(year, cpue))+
    geom_point(shape=1, alpha=0.25, size=0.5)+
    geom_point(data=data.set[data.set$effort.mins<50,], shape=1, alpha=0.25, size=0.75, col='red', position = position_nudge(x=0.25))+
    geom_point(data=data.set[data.set$effort.mins>109,], shape=1, alpha=1, size=0.75, col='blue', position = position_nudge(x=-0.25))+
    #scale_x_continuous(breaks = seq(0,125, by=5), guide = guide_axis(angle = 90))+
    xlab("year")+ylab("CPUE")
  p <-  ggExtra::ggMarginal(p, type="histogram", margins = "y", binwidth=1)
  print(p)

  if(! is.na(filename)){
  #filename <- paste0("../../plots/cpue_vs_year_outliers_scatterplot", ".png")
  ggsave(filename = filename,plot = p, height = 4, width = 8, dpi = 600)
  }

}#END plotCPUEVsYear





plotCPUEcompare_scatter <- function(data.combined, filename=NA){
  # validate to FOS cpue ----------------------------------------------------

  p <- ggplot(data=data.combined, aes(cpue,cpue.fos))+
    geom_point()+
    geom_abline(intercept = 0, slope = 1, col='red')+
    xlab("Inseason Package Estimate CPUE")+ylab("FOS CPUE")
    coord_fixed()


  if(! is.na(filename)){
    #filename <- "cpue_compare_simple.png"
    ggsave(filename, width = 8, height = 8)
  } else {
   print(p)
  }

}#ENDplotCPUEcompare_scatter


# range(data.combined$ratio[data.combined$cpue !=0], na.rm = TRUE)
# limits <- c(0.95,1.05)
# hist(data.combined$ratio[data.combined$ratio>limits[1] & data.combined$ratio<limits[2]], breaks = seq(limits[1],limits[2], by=0.001))
# dat.hist <- hist(data.combined$ratio[data.combined$ratio>limits[1] & data.combined$ratio<limits[2]], breaks = seq(limits[1],limits[2], by=0.001), plot=F)
# cbind(dat.hist$breaks, dat.hist$counts)
#
#
# data.review <- data.combined[!is.na(data.combined$ratio) & (round(data.combined$ratio,3)<0.99 | round(data.combined$ratio,3)> 1.01),]
# View(data.review[order(data.review$ratio, rev(data.review$year)),])
# writeClipboard(knitr::kable(data.review[data.review$fe_start_date<=as.Date("1955-07-21"),1:8], row.names=F))
#
# View(data.combined[data.combined$diff !=0 & data.combined$ratio<.7,])
# writeClipboard(knitr::kable(data.tyee.raw.sub[data.tyee.raw.sub$fe_start_date>=as.Date("1955-07-17") & data.tyee.raw.sub$fe_start_date<=as.Date("1955-07-21"),c(10:12,28)], row.names = F))
#
# hist(data.combined$ratio[data.combined$ratio<2], breaks = seq(0,2, by=0.01))
# data.hist <- hist(data.combined$ratio[data.combined$ratio<2], breaks = seq(0,2, by=0.01), plot = F)
# data.hist$breaks[data.hist$counts>400]

#
# ggplot(data=data.combined[data.combined$effort.hrs<20,], aes(year, effort.hrs))+
#   geom_boxplot()+
#   theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
#
#
# ggplot(data=data.combined, aes(effort.hrs, ratio))+
#   geom_point(shape=1, size=1, alpha=0.5)
#
# #the only 4 cases from the whole series that are substantially different
# ggplot(data=data.combined[data.combined$year==1955,], aes(date.standard, cpue))+
#   geom_line(col='green')+
#   geom_point(col='green', size=1)+
#   geom_line(data=data.combined[data.combined$year==1955,], aes(date.standard, value), col='red')+
#   geom_point(data=data.combined[data.combined$year==1955,], aes(date.standard, value), col='red', shape=1, size=1.5)
# ggsave("cpue1955_timeseries.png", width = 8, height = 4)
#
#
#
#
#
#
# # calc daily proportions --------------------------------------------------
#
#
# data.cpue <- calcDailyProportions(x = data.cpue)
# dim(data.cpue.list$data.day$data.daily)
# dim(data.cpue)
#
#
#
# #excluding the values on 2010-06-10 as it's quite high, 1991-06-16 is low
# #data.cpue <- data.cpue[! data.cpue$date %in% as.Date(c("2010-06-10", "1991-06-16")),]
#
# #use data starting June 10
# data.cpue.sub <- data.cpue
# data.cpue.sub <- data.cpue[data.cpue$date.standard >= as.Date("2021-06-10"),]
#
# #logit can't use 0 or 1:
# #data.tyee.daily.long.sub <- data.tyee.daily.long.sub[! data.tyee.daily.long.sub$prop.cumul %in% 0:1,]
#
# dim(data.cpue)
# dim(data.cpue.sub)
#
# # evaluate outliers in cumulative proportions ---------------------------------
#
#
# #___________________________________
# # boxplot plots
#
#
# outliers.list <- with(data.cpue.sub, by(data.cpue.sub, date.standard, FUN=function(x){
#
#   res <- apply(data.frame(probs=c(0.25,0.75)), 1, function(p){
#   #  if(x$date.standard=="2021-09-09") browser()
#    df.tmp <- data.frame(date.standard=unique(x$date.standard),probs=p, q=quantile(x$prop.cumul,  probs = p), iqr=IQR(x$prop.cumul))
#    if(p==0.25){
#     df.tmp$threshold <- min(c(1,x$prop.cumul[x$prop.cumul> df.tmp$q + -1.5 *df.tmp$iqr]))
#    } else {
#     df.tmp$threshold <- max(x$prop.cumul[x$prop.cumul< df.tmp$q +  1.5 *df.tmp$iqr])
#     df.tmp$threshold[df.tmp$threshold<0] <- 1
#    }
#    df.tmp
#   })
#   do.call(rbind, res)
# } ))
#
# outliers.df <- do.call(rbind, outliers.list)
#
# outliers.wide <- reshape(outliers.df[c("date.standard", "probs", "threshold")], direction = 'wide', idvar = "date.standard", timevar = 'probs')
#
# # outliers.df$threshold <- apply(outliers.df,1, function(x){
# #
# #   x <- as.list(x)
# #   x <- lapply(x, type.convert, as.is=TRUE)
# #   #if(x$date.standard=="2021-07-15") browser()
# #   if(x$probs==0.25) threshold <- max(x$prop.cumul.extreme, x$prop.cumul + sign(round(x$probs)-.5)* 1.5 *x$iqr)
# #   if(x$probs==0.75) threshold <- min(x$prop.cumul.extreme, x$prop.cumul + sign(round(x$probs)-.5)* 1.5 *x$iqr)
# #   threshold
# # } )
# # View(outliers.df)
#
# #data.threshold <- outliers.df[outliers.df$probs==0.75, c("date.standard.f", "threshold")]
#
# # labels <- rep("", length(unique(data.cpue.sub$date.standard)))
# # dates <- format(unique(data.cpue.sub$date.standard), "%b-%d")
# # breaks <- seq(1, length(dates),by=7)
# # labels[breaks] <- dates[breaks]
#
# p <- ggplot(data=data.cpue.sub, aes(date.standard, prop.cumul))+
#   geom_boxplot(aes(group=date.standard))+
#   geom_line(data=outliers.df, aes(date.standard, threshold, group=probs), col='red')+
#   scale_x_date(breaks = "4 days",date_labels = "%b-%d", guide = guide_axis(angle = 90))+
#   scale_y_continuous(breaks = seq(0,1, by=0.1))+
#   ylab("Cumulative Proportion of Run")
# p
# filename <- paste0("../../plots/CumulativeProbabilityofRun_boxplot", ".png")
# ggsave(filename = filename,plot = p, height = 8, width = 16, dpi = 600)
#
#
#
# #if removing outliers
# # data.cpue.sub <- merge(data.cpue.sub, outliers.wide, by="date.standard")
# # dim(data.cpue.sub)
# # data.cpue.sub <- data.cpue.sub[data.cpue.sub$prop.cumul < data.cpue.sub$threshold.0.75 & data.cpue.sub$prop.cumul > data.cpue.sub$threshold.0.25,]
# dim(data.cpue.sub)
#
#
#
# # fit binomial regression -------------------------------------------------
#
#
#
# glm.fit <- glm(prop.cumul~od, data=data.cpue.sub, family =binomial )
#
# # data.tyee.daily.long$residuals <- glm.fit$residuals
# # data.tyee.daily.long$fit <- predict(glm.fit)
#
# predict.df <- data.frame(od=seq(min(data.cpue.sub$od), max(data.cpue.sub$od)))
# predict.df$date.standard <- utilizer:::calcDate(predict.df$od)
# glm.fit.predict <- predict(glm.fit, newdata = predict.df,  se.fit = TRUE)
# predict.df$mean.logit <- glm.fit.predict$fit
# predict.df$mean <- logit.inverse(glm.fit.predict$fit)
# #predict.df$sd.logit <- aggregate(cbind(resid.sd=residuals)~od, data = data.tyee.daily.long, sd)$resid.sd
# critval <- qnorm(p = 0.975)
#
# #as the fit has negative values, the sqrt fails.
# #predict.df$se_pi <- sqrt(glm.fit.predict$se.fit ^ 2 + glm.fit.predict$fit * glm.fit.predict$residual.scale ^ 2)
# # predict.df$lowerPI <- logit.inverse(glm.fit.predict$fit - critval*predict.df$se_pi)
# # predict.df$upperPI <- logit.inverse(glm.fit.predict$fit + critval*predict.df$se_pi)
#
#
# predict.df$lowerCI <- logit.inverse(glm.fit.predict$fit - critval*glm.fit.predict$se.fit)
# predict.df$upperCI <- logit.inverse(glm.fit.predict$fit + critval*glm.fit.predict$se.fit)
#
#
#
#
# # data.tyee.daily.long$prop.cumul.logit <- logit(data.tyee.daily.long$prop.cumul)
# # data.tyee.daily.long$prop.cumul.logit <- SkeenaCNinseason:::logit(data.tyee.daily.long$prop.cumul)
# #
# # head(data.tyee.daily.long)
# # data.daily.stats <- by(data.tyee.daily.long, data.tyee.daily.long$od,FUN= function(x) data.frame(od=unique(x$od), logit.mean=mean(x$prop.cumul.logit, na.rm = TRUE), logit.sd=sd(x$prop.cumul.logit, na.rm = TRUE)))
# #
# # data.daily.stats <- do.call(rbind, data.daily.stats)
# # data.daily.stats$mean <- logit.inverse(data.daily.stats$logit.mean)
# # data.daily.stats$lowerCI <- logit.inverse(qnorm(p = .025, data.daily.stats$logit.mean, data.daily.stats$logit.sd))
# # data.daily.stats$upperCI <- logit.inverse(qnorm(p = .975, data.daily.stats$logit.mean, data.daily.stats$logit.sd))
# #
# # data.daily.stats$date.standard <- utilizer:::calcDate(data.daily.stats$od)
# # data.daily.stats$date.standard.f <- as.factor(data.daily.stats$date.standard)
# #
# # head(data.daily.stats)
#
#
#
#
# # quantiles ---------------------------------------------------------------
# #results by day across all years
#
# data.quants <- by(data.cpue.sub, data.cpue.sub$od, FUN=function(x) {
#   probs=c(0.025, 0.5,0.975)
#   q=quantile(x$prop.cumul, probs=probs)
#   prop.tmp <- x$prop.cumul
#   prop.tmp[prop.tmp==0] <- min(prop.tmp[prop.tmp>0])/2
#   prop.tmp[prop.tmp==1] <- max(prop.tmp[prop.tmp<1])+(1-max(prop.tmp[prop.tmp<1]))/2
#   x$prop.tmp <- prop.tmp
#   prop.logit <- logit(prop.tmp)
#   #if(x$od==167) browser()
#   q.logit <- logit.inverse(qnorm(probs, mean(prop.logit), sd(prop.logit)))
#   data.frame(od=unique(x$od), date.standard=unique(x$date.standard),probs=probs, q=q, q.logit=q.logit, mean(prop.logit), sd(prop.logit))
#   })
# data.quants <- do.call(rbind, data.quants)
# data.quants$probs.f <- as.factor(data.quants$probs)
# dim(data.quants)
#
#

# # run ranges --------------------------------------------------------------
# #
# # data.run.range <- by(data.cpue.sub, data.cpue.sub$year,FUN = function(x) {
# #   data.frame(year=unique(x$year),day.range= x$od[which.min(abs(x$prop.cumul - 0.9))]- x$od[which.min(abs(x$prop.cumul - 0.1))])
# # })
# # data.run.range <- do.call(rbind, data.run.range)
# #data.run.range <- merge(data.percentile.date, data.percentile.date, by='year')
#
# data.run.range <- merge(data.percentile.date[,c("year", "p.str", "date.standard")], data.percentile.date[,c("year", "p.str", "date.standard")], by='year')
# data.run.range$day.range <- as.numeric((data.run.range$date.standard.x- data.run.range$date.standard.y))
# data.run.range$prange <- paste(data.run.range$p.str.x, data.run.range$p.str.y, sep="-")
#
# head(data.run.range)
#
# # proportion stats --------------------------------------------------------
#
# data.cpue.sub$prop.cumul.round <- round(data.cpue.sub$prop.cumul,2)
# range(data.cpue.sub$prop.cumul)
#
# data.quants.prop <- by(data.cpue.sub, data.cpue.sub$prop.cumul.round, FUN=function(x) {
#   probs=c(0.025, 0.5,0.975)
#   q=quantile(x$od, probs=probs)
#   data.frame(p=unique(x$prop.cumul.round), probs=probs, od=q)
# })
# data.quants.prop <- do.call(rbind, data.quants.prop)
# data.quants.prop$date.standard <- utilizer:::calcDate(data.quants.prop$od)
# data.quants.prop$probs.f <- as.factor(data.quants.prop$probs)
#
# ggplot(data=data.quants.prop, aes(p,date.standard, group=probs))+
#   geom_line()
#
# data.quants.prop.wide <- reshape(data.quants.prop[,c("p", "probs", "date.standard")], direction = 'wide', idvar = 'p', timevar = 'probs' )
#
#
# # plots --------------------------------------------------------------------
#
#
#
#
#
#
#
# # __________________________________________________________
# #lattice plot, facet is year, daily cpue time series
#
# p <- ggplot(data=data.cpue.sub, aes(date.standard, value))+
#   geom_point(size=0.3)+
#   geom_path()+
#   ylab("CPUE")+
#   facet_wrap(vars(year), ncol = 4)
# p
# ggsave("../../plots/cpue_by_year_timeseries_outliersexcluded.png", p, width = 8, height = 20)
#
#
#
#
#
#
# # __________________________________________________________
# # this plots all daily values of cumulative proportion of cpue and 95% quantiles
#
# plotcumulative <- function(step.vec){
#  for(step in step.vec){
#   p <- ggplot(NULL)+
#   geom_point(data=data.cpue.sub, aes(date.standard, prop.cumul), size=0.5)+
#    scale_x_date(date_breaks = "10 day",date_minor_breaks = "2 day", date_labels = "%b-%d", guide = guide_axis(angle = 90), name = "Date in 2021")+
#   ylab("Cumulative Probability of Run")
#  if(step>=2) p <- p + geom_line(data=predict.df, aes(date.standard, mean), size=1)
#  if(step>=3) p <- p + geom_ribbon(data = predict.df, aes(x=date.standard, ymin=lowerCI, ymax=upperCI), alpha = 0.2 , fill='blue' )
#  if(step>=4) p <- p + geom_line(data=data.quants, aes(x=date.standard, y=q, group=probs.f))
#  if(step>=5) p <- p + geom_line(data=data.quants, aes(x=date.standard, y=q.logit, group=probs.f), col='red')
#   # #geom_point(data=data.quants.prop[data.quants.prop$probs %in% c(.025, .975),], aes(x=date.standard, y=p, group=probs.f),shape=1, col='blue')+
#  if(step>=6) p <- p + geom_errorbarh(data=data.quants.prop.wide, aes(y=p, xmin=date.standard.0.025, xmax=date.standard.0.975), col='blue')
#   #geom_errorbar(data=data.daily.stats, aes(x=date.standard.f, ymin=lowerCI, ymax=upperCI), width=.2, col='red')+
#
#  filename <- paste0("../../plots/CumulativeProbabilityofRun_outliersexcluded",step, ".png")
#  ggsave(filename = filename,plot = p, height = 8, width = 16, dpi = 600)
#   }
# }#END plotcumulative
#
#
#
# plotcumulative(1:6)
#
#
# # __________________________________________________________
# #histograms, facet by day, of the frequencies of run proportions
#
# p <- ggplot(data=data.cpue.sub,aes(prop.cumul))+
#   geom_histogram()+
#   facet_wrap(vars(format(date.standard, "%b-%d")))
# p
#
#
#
# # __________________________________________________________
# # plot time series of percentile dates, one panel per percentile
#
# p <- ggplot(data=data.percentile.date, aes(year, date.standard))+
#   geom_vline(xintercept = c(1987.5, 2007.5), col='grey')+
#   geom_point()+
#   geom_line()+
#   #scale_y_date(date_breaks = "6 days", date_labels = "%b-%d")+
#   facet_grid(vars(p.f), scales = "free_y")
# p
# ggsave("../../plots/percentile_dates_timeseries_outliersexcluded.png",plot = p, height = 11, width=6,dpi=600)
#
#
# # __________________________________________________________
# # plot time series of run day range 10th to 90th percentile
#
#
# p <- ggplot(data=data.run.range[data.run.range$prange=="0.9-0.1",], aes(year, day.range))+
#   geom_point()+
#   geom_line()+
#   #scale_y_continuous(breaks = seq(20,60,4))+
#   theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))+
#   ggtitle("Day range from 10th to 90th percentile")
# p <- ggExtra::ggMarginal(p, type="histogram", margins = "y",bins=12)
# p
# ggsave("../../plots/runtiming_range_timeseries_outliersexcluded.png",plot = p, height = 4.5, width=11,dpi=600)
#
#
#
#
# # __________________________________________________________
# #facet is percentile, day range vs date
# data.day.range90p <- data.run.range[data.run.range$prange=="0.9-0.1",c("year", "day.range")]
# data.run.range.combo <- merge(data.run.range[,c("year", "p.str.x", "date.standard.x")], data.day.range90p, by='year')
# data.run.range.combo$p.f <- factor(data.run.range.combo$p.str.x, levels = c((1:5)/10, "mode", (6:9)/10))
# p <- ggplot(data=data.run.range.combo, aes(date.standard.x, day.range))+
#   ylab("10th to 90th Percentile Day Range")+xlab("Standard Date")+
#   geom_point()+
#   #geom_line()+
#   facet_wrap(vars(p.f),ncol = 2)
# p
# ggsave("../../plots/runtiming_range_Vs_percentileDate_scatter_outliersexcluded.png",plot = p, height = 11, width=8,dpi=600)
#
#
# # __________________________________________________________
# #lattice plot percentile dates against all other percentile dates
#
#
#
# p <- ggplot(data=data.run.range, aes(date.standard.x, date.standard.y))+
#   geom_point(size=0.75, shape=1)+
#   theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))+
#   facet_grid(p.str.y~p.str.x)
# p
# ggsave("comparePercentileDates_scatter.png",plot = p, width = 18, height = 9)
#
#
# # __________________________________________________________
# #lattice plot, facet is percentile's compared, plot day range vs initial percentile dates
#
#
#
# p <- ggplot(data=data.run.range, aes(date.standard.x, day.range))+
#   geom_point(size=0.75, shape=1)+
#   ylab("Day range")+
#   theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))+
#   facet_wrap(vars(prange))
# p
# ggsave("comparePercentileDates_vsRange_scatter.png",p, width = 18, height = 9)
#
#
#
#
#
