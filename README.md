# inseason

An R package of tools that aid with forecasting of (generally salmon) in-season run size.

## Installation

This package relies on the [FOSer package](https://gitlab.com/MichaelFolkes/FOSer) for querying the Tyee test fishing data from DFO's FOS. This only works if within the DFO network. See the readme file in the FOSer package for additional details.

To install in R:
    
```
install.packages("remotes") 
remotes::install_git("https://gitlab.com/MichaelFolkes/inseason")
```

Load `inseason` into memory:
```
require(inseason)
```

## Demo scripts!

The function `writeScript` will make it easier to start things. The help file has an example:
```
?inseason::writeScript()
```

In the example we can see how to seek what demo scripts are available:
```
demo(package = "inseason")
```

To save and open a specific demo script (this one named "demo_aa"):
```
writeScript("demo_aa")

```

Here is how to open a copy of the script that queries FOS:
```
writeScript("demo_queryFOS_tyee")

```

Here is how to open a copy of the script that queries FOS and cleans the data:
```
writeScript("demo_queryFOS_tyee_with_data_cleanup")

```
