

# comments ----------------------------------------------------------------

#author: Michael Folkes
#date: "Fri Apr 22 08:51:42 2022"

#comment: This script queries FOS for the Tyee test fishing data in five tables. The results are saved to an RDS file.

#the imported file, fos_id.csv, includes a user's id and pw to log onto fos. the file will have this structure, where the second row is the user's id and pw:
#username,password
#id,pw


# setup -------------------------------------------------------------------

rm(list = ls())

# packages ----------------------------------------------------------------

require(inseason)

# variables ---------------------------------------------------------------


#this config file includes at least three variables, each being a path
#this file is user specific, as such it is in the .gitignore and won't be followed by git
settings <- yaml::read_yaml("user.settings.config")



# data:query FOS ----------------------------------------------------------

#source of username and pw for FOS:
userid <- yaml::read_yaml(settings$userid.filepath)
#the userid text file would have variables 'username' and 'password' that are your fos account credentials:
#list(username= "user", password= "mypassword")


data.fos.raw <- FOSer::queryFOStyee_tables(connect_file_config = settings$config.filepath, userid = userid)
data.fos.raw$data$set <- data.fos.raw$data$set[order(data.fos.raw$data$set$PARENT_FE_ID),]

filename <- paste0(settings$path.export, "/data.fos.raw_", Sys.Date(), ".Rds")
saveRDS(object = data.fos.raw, file = filename)
