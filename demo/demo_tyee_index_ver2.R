
# comments ----------------------------------------------------------------
metadata <- list(
  filename= "demo_tyee_index_ver2.R",
 author= "Michael Folkes",
 datetime= "Mon Jul 11 09:20:56 2022",
 comments= "this script estimates the tyee cpue index. the defaults are for chinook but it can be modified for other species, with appropriate conversion rates for monofilament to multifilament. User should run the script demo_queryFOS_tyee_with_data_cleanup.R and use its processed output (Rds file or xlsx) in this script. The steps are:
- import processed data (from Rds or xlsx)
- calculate per set effort (default is to exclude bad sets and sets with incomplete panel times)
- calculate per set cpue. the default species is CN but other species can be chosen. see the function help.
- calculate the daily cpue (ie the tyee index). the default method matches that for chinook. that is, the mono to multifilament adjustment is 0.635 and monofilament is excluded for 1995-2001.  the function arguments can accomodate variations on that (different adjustment and inclusion of early monofilament years)
- the final section includes option to import the FOS derived daily index to validate output. user will have to run the FOS report 'Skeena Tyee Indices for a Year and Species' to obtain all the annual text files of fos output.
-
 "
)

#lapply(metadata, cat)

# packages ----------------------------------------------------------------

require(inseason)
require(readxl)


# data:paths --------------------------------------------------------------

settings <- yaml::read_yaml("user.settings.config")


# data:import from xlsx -------------------------------------------------------------

filepath <- list.files(settings$path.export, pattern = "xlsx$", full.names = TRUE)
#chose which file is desired (should have 'data.fos.processed' in filename):
filepath <- filepath[1]
filepath

sheets.names <- excel_sheets(filepath)
data.fos <- lapply(sheets.names, function(x){
  data.tmp <- read_excel(sheet = x, path = filepath)
  as.data.frame(data.tmp)
} )

#the new naming convention will match the data frames found in the rds file
names(data.fos) <- paste0("data.", sheets.names)
names(data.fos) <- gsub("_", ".", names(data.fos))
data.fos <- list(comments=data.fos$comments, data=data.fos[2:length(data.fos)])

#when importing with readxl, the set times are characters as need to specify in PDT, so convert to posixct
net.time.colnames <- names(data.fos$data$data.panel.effort)[grep("net_start|net_full", names(data.fos$data$data.panel.effort), ignore.case = TRUE)]
net.time.colnames

for (column in net.time.colnames) {
    data.fos$data$data.panel.effort[, column] <- as.POSIXct(data.fos$data$data.paneleffort[, column])
}

names(data.fos$data)
str(data.fos$data$data.panel.effort)


# data:import from RDS ----------------------------------------------------
#this is alternate source (choose this or the prior xls)

# filepath <- list.files(settings$path.export, pattern = "data\\.fos\\.processed.*Rds$", full.names = TRUE, ignore.case = TRUE)
# #chose which file is desired (should have 'data.fos.processed' in filename):
# filepath
# filepath <- filepath[length(filepath)]
# data.fos <- readRDS(filepath)
# names(data.fos$data)

# main --------------------------------------------------------------------
#imported table column names are all uppercase as is found in FOS. Will set to lowercase
data.fos.uppercase <- data.fos
names(data.fos)
names(data.fos$data)

data.fos$data <- lapply(data.fos$data, function(x) {colnames(x) <- tolower(colnames(x)); x})
lapply(data.fos$data, names)

list2env(data.fos$data)

#colnames(data.day)[colnames(data.day)=="vessel"] <- "vessel_name"

# calculate effort & CPUE ------------------------------------------------------
data.fos$data.set.cpue <- data.set.cpue <- calcSetCPUEwrapper(data.vesselday = data.vesselday, data.panel.effort = data.panel.effort, data.panel.catch = data.panel.catch)
#limit to chinook:
data.set.cpue <- data.set.cpue[data.set.cpue$species_cde ==124,]

#bring in the seal data
#data.set.cpue <- merge(data.set.cpue, data.set[,c("parent_fe_id", "seals")])

# calculate daily index ------------------------------------------------------
#this is the daily index
data.fos$data.cpue.index <- data.cpue.index <- calcDailyCPUE(data.set.cpue)
str(data.fos$data.cpue.index)


#write to csv
filepath <- paste(settings$path.tmp, "inseason_derived_daily_tyee_index_chinook.csv", sep = "/")

utilizer::writeData(x = data.cpue.index$data.daily, filename = filepath, comments = c("column cpue includes the monofilament cpue, adjusted to mutifilament equivalent","column cpue.original is a blend of unadjusted monofilament cpue and mutilfilament cpue", "date.standard is date values adjusted to a chosen year (default is 2021), these are initially corrected for leap year", "od is ordinal day (day of year)"))


data.cpue.index <- list()
filepath <- paste(settings$path.tmp, "inseason_derived_daily_tyee_index_chinook.csv", sep = "/")
data.cpue.index$data.daily <- read.csv(filepath, comment.char="#")
str(data.cpue.index$data.daily)
data.cpue.index$data.daily$fe_start_date <- as.Date(data.cpue.index$data.daily$fe_start_date)

# validate to FOS index ---------------------------------------------------
#compare to posted daily cpue index

#these text files are output of the FOS report named:   Skeena Tyee Indices for a Year and Species
#note the need to rename them from xls to .txt as they are actually text files, not xls
filepath <- list.files(settings$path.fos.report.index, pattern = "txt$", ignore.case = TRUE, full.names = TRUE)
filepath.df <- data.frame(filepath=filepath)
filepath.df$year <- lapply(strsplit(tools::file_path_sans_ext(basename(filepath.df$filepath)), split = "_"), function(x)x[3])

data.tyee.fos.index <- inseason:::readTyeeIndex(filename = filepath)
data.tyee.fos.index <- data.tyee.fos.index[order(data.tyee.fos.index$date),]
str(data.tyee.fos.index)
tail(data.tyee.fos.index)

data.tmp <- merge(data.cpue.index$data.daily, data.tyee.fos.index, by.x="fe_start_date", by.y="date")
data.tmp$cpue <- round(data.tmp$cpue,2)
dim(data.cpue.index$data.daily)
dim(data.tmp)

#show results where cpue's don't match:
res <- data.tmp[data.tmp$cpue != data.tmp$cpue.fos,]
nrow(res)
names(res)
View(res[,c(names(res), "cpue")])


require(ggplot2)

View(data.tmp[data.tmp$cpue.fos>13.5,])
dev.new()
ggplot(data.tmp, aes(cpue.fos, cpue))+
  geom_abline(slope=1, col="grey")+
  geom_point(size=0.25)+
  geom_point(data=res, col="red", size=0.5)+
  geom_point(data=res[res$year.x>=1970,], col="orange", size=0.5)+
geom_point(data=res[res$year.x==2022,], col="blue", size=0.5)+
  xlab("FOS CPUE Daily Index")+ylab("Inseason Pakckage CPUE Daily Index")
filename <- "cpue_compare_scatterplot.png"
filepath <- paste(settings$path.img, filename, sep="/")
ggsave(filepath, height = 8, width = 8, dpi=400)
shell.exec(filepath)
