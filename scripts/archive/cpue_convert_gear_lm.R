

names(data.set.tides)
res <- aggregate(cpue~fe_start_date+year+net_config_nme, data.set.tides[data.set.tides$year %in% 1995:2001,], mean)
res$net_config_nme <- gsub(" ", "", res$net_config_nme)
res <- reshape(res, direction = "wide", idvar = c("year", "fe_start_date"), timevar = "net_config_nme")
tail(res)
unique(res$year)
fit.res <- lm(cpue.MultiNylon~cpue.MonoNylon, data=res)
summary(fit.res)

fit.res <- lm(cpue.MonoNylon~cpue.MultiNylon, data=res)
summary(fit.res)
