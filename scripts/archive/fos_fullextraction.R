

require(readxl)


data.tyee <- read_excel("C:\\Users\\folkesm\\Documents\\Projects\\salmon\\skeena\\inseason_forecasting\\data\\fos_reports\\full_extraction\\FOS_SkeenaTestFishery_CatchData_1950-present.xlsx")
data.tyee <- as.data.frame(data.tyee)

data.tyee$date <- as.Date(data.tyee$NET_START_OUT)
data.tyee$year <- as.integer(format(data.tyee$date, "%Y"))


str(data.tyee)


unique(data.tyee$FE_WATER_TEMP)

unique(data.tyee$GAUGE_HGT)

#no secci values
unique(data.tyee$SECCHI_AM)

#mesh probably shouldn't have value=multi, check if there's catch for that record (probably is)
unique(data.tyee$MESH)
#before 1992 no records that are specific to mesh size:
table(data.tyee$MESH, data.tyee$year)


unique(data.tyee$SPECIES_COMMON_NME)

unique(data.tyee$SEX)

unique(data.tyee$MATURITY)
unique(data.tyee$KEPT_REL)

sort(unique(data.tyee$CATCH_QTY))


str(data.tyee)
head(data.tyee)


table(data.tyee$year[!is.na(data.tyee$SECCHI_AM)])

unique(data.tyee$SECCHI_AM)

date.cols <- c("NET_START_OUT", "NET_FULL_OUT", "NET_START_IN", "NET_FULL_IN" )
lapply(date.cols, function(x) length(data.tyee[,x][is.na(data.tyee[,x])]))
#121 cases of an empty set times
length(data.tyee$NET_START_OUT[is.na(data.tyee$NET_START_OUT)])

#this matches the screen shot
View(data.tyee[! is.na(data.tyee$NET_START_OUT) & data.tyee$NET_START_OUT==as.POSIXct("2021-09-11 12:42", tz = "UTC"),])

#using 2021 as example, if a panel lacks catch then it's excluded. following this logic if a set lacks catch, it's likely excluded.
res <- with(data.tyee[data.tyee$year==2021,], table(SET_NO, PANEL_NO))
res

#need fields:
#catch report ID
#fishing event id
#comments
