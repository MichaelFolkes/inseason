

#example

days <- seq(utilizer:::calcOrdinalDay("2023-5-1"), utilizer:::calcOrdinalDay("2023-8-31"))
day.start <- utilizer:::calcOrdinalDay("2023-6-20")
med <- utilizer:::calcOrdinalDay("2023-7-10")
x <- pnorm(q = days, med, 15)
x <- cumsum(x)/sum(x)
x <- c(0,x)
df <- data.frame(days=days, p.day=diff(x), p.cum=x[-1])

range.v <- c(0,max(df$p.cum))
par(xpd=F)
with(df, plot(days,p.cum, ylim=range.v, xaxt='n'))
grid()
#abline(h=seq(0,1, by=0.025), lty = "dotted", col="grey")
abline(h=seq(0,1, by=0.05), lty = "dotted", col="black")
d <- seq(min(df$days), max(df$days), by=5)
par(xpd=TRUE)
text(d, -0.1, format(utilizer:::calcDate(d), "%b-%d"), srt=90)

run <- 1e5
df$cpue <- df$p.day*run
df[df$days==day.start,]
df$cpue.view <- df$cpue
df$cpue.view[df$days<day.start] <- NA

p.remain <- 1-df$p.cum[df$days==(day.start-1)]
sum(df$cpue.view, na.rm = TRUE)/p.remain



require(mice)
require(readxl)
require(inseason)

filename <- "C:/Users/folkesm/Documents/Projects/salmon/skeena/inseason_forecasting/data/temporary/data.set.tides.fullseries.Rds"
data.set.tides <- readRDS(filename)
str(data.set.tides)
range(data.set.tides$fe_start_date)


data_mono_tide_impute <- function(data, data.popan){

  data.set <- data[grepl("mono", data$net_config_nme, ignore.case = TRUE) & data$year>=2001,]
  fit.res <- loess(cpue~rate.avg,  data = data.set)

  data.set$cpue.pred <- as.numeric(predict(fit.res, data.set[, "rate.avg", drop=FALSE]))
  data.set$cpue.pred[data.set$cpue.pred<0] <- 0

  base.rate <- 0
  cpue.pred.base <-  as.numeric(predict(fit.res, data.frame(rate.avg=base.rate)))

  data.set$cpue.scaler <- data.set$cpue.pred/cpue.pred.base
  data.set$cpue.adj <- data.set$cpue/data.set$cpue.scaler
  data.set$cpue.adj[is.infinite(data.set$cpue.adj)] <- 0

  data.daily <- aggregate(cpue.adj~fe_start_date+od+year, data=data.set, mean)

  res <- with(data.daily, by(data.daily, list(year), function(x)data.frame(fe_start_date=x$fe_start_date, cpue.cumsum= cumsum(x$cpue.adj))))
  res <- do.call(rbind, res)
  data.daily <- merge(data.daily, res)

  #___________

  data.daily$cpue <- data.daily$cpue.adj
  data.proportions <- calcDailyProportions(data.daily)
  data.proportions$data.yearly.daily <- data.proportions$data.yearly.daily[data.proportions$data.yearly.daily$year !=2023,]

  #of the total witnessed run, this is the lowest proportion seen
  min.by.year <- aggregate(prop.cumul~year, data=data.proportions$data.yearly.daily, min)
  #res <- do.call(rbind, lapply(split(data.proportions$data.yearly.daily, data.proportions$data.yearly.daily$year), function(x)data.frame(year=unique(x$year), prop.cumul=min(x$prop.cumul))))
  #res[order(res$prop.cumul),]

  fn <- ecdf(min.by.year$prop.cumul)
  fn(0.0075)

  years.incomplete <- min.by.year$year[min.by.year$prop.cumul>=0.0075]
  years.complete <- min.by.year$year[min.by.year$prop.cumul<0.0075]


  dat.tmp <- data.proportions$data.yearly.daily[data.proportions$data.yearly.daily$year %in% years.complete,]

  stats.tmp <- with(dat.tmp, by(dat.tmp, list(year), function(x){

    x.tmp <- rep(x$od, round(x$cpue.adj*1000))
    df.tmp <- data.frame(med=median(x.tmp), sigma=sd(x.tmp), p84=quantile(x.tmp, pnorm(1)))
    df.tmp$sigma2 <- df.tmp$p84-df.tmp$med
    df.tmp$sigma3 <- x$od[which.min(abs(pnorm(1)-x$prop.cumul))] -x$od[which.min(abs(0.5-x$prop.cumul))]

    df.tmp$cv <- df.tmp$sigma/df.tmp$med
    df.tmp
  }))

  stats.tmp <- do.call(rbind, stats.tmp)

  NLL <- function(pars, x, priors=NULL) {
    est <- dnorm(x = x$od, mean = pars[1], sd = pars[2])
    if(is.null(priors)){
      prior.likelihoods <- list(1)
    }else{
      prior.likelihoods <- lapply(names(priors), function(priors.name){
        dnorm(pars[priors.name], priors[[priors.name]]$mu, priors[[priors.name]]$sd)
        dunif(pars[priors.name], priors[[priors.name]]$min, priors[[priors.name]]$max)
      })
    }

    r <- log(dnorm(mean = x$prop, est))+log(unlist(prior.likelihoods))
    -sum(r, na.rm = TRUE)
  }#END NLL


  data.tmp <- data.proportions$data.yearly.daily[data.proportions$data.yearly.daily$year %in% years.incomplete,]

  res <- lapply(split(data.tmp, data.tmp$year), function(x){
    mode <- x$od[x$prop==max(x$prop)]
    priors <-  list(mu=list(mu=mean(stats.tmp$med), sd= sd(stats.tmp$med), mu.cv=0.25))
    priors <-  list(mu=list(mu=160, sd= 20, min=150, max=200))
    #priors <- NULL

    print(unique(x$year))

    mle <- optim(par = c(mu = mode, sigma = 5),  fn = NLL, x= x, priors=priors, control = list(maxit=4e4),method = "L-BFGS-B", lower = c(100, 0),upper = c(200, 100))

    if(is.null(priors)) priors <- list(mu.prior=NA, sigma.prior=NA)
    data.frame(year=unique(x$year),t(mle$par), as.data.frame(priors))
  })


  data.parameters <- do.call(rbind, res)

  data.imputed <- lapply(years.incomplete, function(year){

    data.tmp <- data.proportions$data.yearly.daily[data.proportions$data.yearly.daily$year %in% year,]
    od <- 130:270
    density.tmp <- data.frame(od=od, den=dnorm(od, data.parameters$mu[data.parameters$year==year], data.parameters$sigma[data.parameters$year==year]))
    cumul.cpue.adj <- max(data.tmp$cpue.cumul)/sum(density.tmp$den[density.tmp$od %in% data.tmp$od])
    density.tmp$cpue.impute <- density.tmp$den*cumul.cpue.adj
    data.tmp <- merge(data.tmp, density.tmp, all=TRUE)
    data.tmp$year <- year
    data.tmp
  })

  data.imputed <- do.call(rbind, data.imputed)
  #data.imputed$cpue.impute[data.imputed$od>200] <- NA


  p <-  ggplot(data.imputed, aes(od, cpue))+
    geom_line()+
    geom_line(data=data.imputed, aes(y=cpue.impute), col='blue')+
    geom_line(data=data.imputed[is.na(data.imputed$cpue) & data.imputed$od<200,], aes(y=cpue.impute), col='red')+
    facet_wrap(vars(year))
  print(p)
  browser()

  data.forecasts <- merge(data.proportions$data.yearly.daily, data.forecasts, all = TRUE)
  data.forecasts$prop.f[!is.na(data.forecasts$prop)] <- NA

  data.forecasts <- lapply(split(data.forecasts, data.forecasts$year), function(x){
    if(unique(x$year) %in% c(2001:2008, 2018, 2021)){
      cpue.sum.est <- max(x$cpue.cumul,na.rm = TRUE)/(1-sum(x$prop.f, na.rm = TRUE))
      x$cpue[is.na(x$cpue)] <- cpue.sum.est*x$prop.f[is.na(x$cpue)]
      x$cpue.cumsum <- cumsum(x$cpue)}
    x
  })
  data.forecasts <- do.call(rbind, data.forecasts)
  data.fitting <- data.forecasts

  data.fitting <- merge(data.fitting, data.popan, all.x=TRUE)
  data.fitting$terminalrun.log <- log(data.fitting$terminalrun)
  data.fitting$cpue.cumsum.log <- log(data.fitting$cpue.cumsum)

  data.fitting$fe_start_date <- utilizer:::calcDate(paste(data.fitting$year, data.fitting$od, sep="-"), format = "%Y-%j")

  data.fitting[,c('year', 'fe_start_date', 'od', 'cpue', 'cpue.cumsum', "cpue.cumsum.log" ,'Kitsumkalum_POPAN', 'Tyee_Kalum_percentage', 'Skeena_terminal-run_POPAN', 'Skeena_escapement', 'terminalrun', 'terminalrun.log')]

}#END data_mono_tide_impute



data.popan <- as.data.frame(readxl::read_excel("C:/Users/folkesm/Documents/Projects/salmon/skeena/inseason_forecasting/data/escapement/Kitsumkalum_Skeena_terminal-run-escapement_POPAN_to2022.xlsx", sheet = "data"))

colnames(data.popan) <- gsub(" +", "",colnames(data.popan))
data.popan$terminalrun <- data.popan$`Skeena_terminal-run_POPAN`
str(data.popan)


data.fitting <- data_mono_tide_impute(data.set.tides, data.popan)
str(data.fitting)

list(
model.lm.tide=list(
    data.input.df.name="data_mono_tide_adj",
    prepData= function(data){ data[order(data$fe_start_date),]},
    fitData=function(data){ lm(terminalrun~cpue.cumsum*od, data=data) },
    #selectNewdata = function(x) {x},
    selectNewdata = function(x) {x},
    forecastDate=function(fit.obj, newdata){
      newdata$forecast <- predict(object = fit.obj, newdata=newdata)
      newdata
    }
  ),
model.lm.tide_impute=list(
  data.input.df.name="data_mono_tide_impute",
  prepData= function(data){ data[order(data$fe_start_date),]},
  fitData=function(data){ lm(terminalrun~cpue.cumsum*od, data=data) },
  selectNewdata = function(x) {x},
  forecastDate=function(fit.obj, newdata){
    newdata$forecast <- predict(object = fit.obj, newdata=newdata)
    newdata
  }
),

model.lm2stage.tide=list(
  data.input.df.name="data_mono_tide_adj",
  prepData= function(data){ data[order(data$fe_start_date),]},
  fitData=function(data){ lm(terminalrun~cpue.cumsum*od+upper, data=data) },
  selectNewdata = function(x) {x},
  forecastDate=function(fit.obj, newdata){
    newdata$forecast <- predict(object = fit.obj, newdata=newdata)
    newdata
  }
),

model.lm2stage.tide_impute=list(
  data.input.df.name="data_mono_tide_impute",
  prepData= function(data){ data[order(data$fe_start_date),]},
  fitData=function(data){
    fit.lower <- lm(terminalrun~cpue.cumsum*od+upper, data=data)
    },
  selectNewdata = function(x) {x},
  forecastDate=function(fit.obj, newdata){
    newdata$forecast <- predict(object = fit.obj, newdata=newdata)
    newdata
  }
),

model.lm.loglog.tide=list(
  data.input.df.name="data_mono_tide_adj",
  prepData= function(data){ data[order(data$fe_start_date),]},
  fitData=function(data){ lm(terminalrun.log~cpue.cumsum.log*od, data=data) },
  selectNewdata = function(x) {x},
  forecastDate=function(fit.obj, newdata){
    newdata$forecast <- exp(predict(object = fit.obj, newdata=newdata))
    newdata
  }
),

model.lm.loglog.tide_impute=list(
  data.input.df.name="data_mono_tide_impute",
  prepData= function(data){ data[order(data$fe_start_date),]},
  fitData=function(data){ lm(terminalrun.log~cpue.cumsum.log*od, data=data) },
  selectNewdata = function(x) {x},
  forecastDate=function(fit.obj, newdata){
    newdata$forecast <- exp(predict(object = fit.obj, newdata=newdata))
    newdata
  }
)

)



data.fitting$data.set$cpue.original <- data.fitting$data.set$cpue
data.fitting$data.set$cpue <- data.fitting$data.set$cpue.adj
plotCPUEVsTide(data.fitting$data.set, year.emph = 2023)

range(data.set.tides$year)
filename <- "cpueVstidechange_mono_only_2004.png"
filepath <- paste(settings$path.img, filename, sep="/")
inseason:::plotCPUEVsTide(data.set.tides[data.set.tides$year>=2001,],year.emph = 2004, filename = filepath)
shell.exec(filepath)


data.fitting$data.daily$cpue <- data.fitting$data.daily$cpue.adj
data.proportions <- calcDailyProportions(data.fitting$data.daily)
str(data.proportions$data.yearly.daily)
do.call(rbind, with(data.proportions$data.yearly.daily, by(data.proportions$data.yearly.daily, INDICES = list(year), function(x) range(x$od))))

str(data.proportions$data.yearly.daily)

data.proportions$data.yearly.daily <- data.proportions$data.yearly.daily[data.proportions$data.yearly.daily$year !=2023,]
data.proportions.wide <- reshape(data.proportions$data.yearly.daily[,c("od", "year",  "prop")], direction = 'wide', idvar = c("od"), timevar='year')
data.proportions.wide <- data.proportions.wide[order(data.proportions.wide$od),]
row.names(data.proportions.wide) <- NULL
colnames(data.proportions.wide) <- gsub("prop.", "year",colnames(data.proportions.wide))
#colnames(data.proportions.wide) <- gsub("prop.cumul.", "year",colnames(data.proportions.wide))
View(data.proportions.wide)
str(data.proportions.wide)
names(data.proportions.wide)


res <- do.call(rbind, lapply(split(data.proportions$data.yearly.daily, data.proportions$data.yearly.daily$year), function(x)cbind(unique(x$year), min(x$prop.cumul))))
res[order(res[,2]),]
hist(res[,2])

quantile(res[,2])
fn <- ecdf(res[,2])
quantile(res[,2], probs = pnorm(0.975))
fn(0.0075)
res[res[,2]>=0.0075,]

aggregate(od~year, data.proportions$data.yearly.daily[!is.na(data.proportions$data.yearly.daily$prop.cumul),], min)

require(ggplot2)
View(data.proportions$data.yearly.daily)
dev.new()


imputed_Data <- mice(data.proportions.wide[,-1], m=2, maxit = 1, visit="monotone", printFlag = F)
summary(imputed_Data)
View(complete(imputed_Data,1))
View(data.proportions.wide)

covariates <- paste0("year", 2009:2016)
formula.str <- paste0("year2001~", "od+",paste0(covariates, collapse = " + "))
data.proportions.wide2 <- data.proportions.wide
data.proportions.wide2[data.proportions.wide2==0 & !is.na(data.proportions.wide2)] <- 1e-6
data.proportions.wide2[data.proportions.wide2==1 & !is.na(data.proportions.wide2)] <- 0.999999
data.proportions.wide.logit <- utilizer:::logit(data.proportions.wide2[,-1])
data.proportions.wide.logit <- data.frame(od=data.proportions.wide[,1], data.proportions.wide.logit)
View(data.proportions.wide.logit)
lm.fit <- lm(as.formula(formula.str), data = data.proportions.wide.logit)
summary(lm.fit)

plot(lm.fit)
days.missing <- data.proportions.wide$od[is.na(data.proportions.wide$year2001)]
newdata <- data.proportions.wide.logit[data.proportions.wide.logit$od %in% days.missing,]
newdata$p <- utilizer:::logit.inverse(predict(lm.fit, newdata = newdata))
newdata$year <- 2001
View(newdata)
str(data.proportions.wide)
data.proportions.wide.infill <- apply(data.proportions.wide, 1, function(x){
  if(is.na(x["year2001"])) x["year2001"] <- mean(x[-1], na.rm = T)
  x
})
data.proportions.wide.infill <- as.data.frame(t(data.proportions.wide.infill))
str(data.proportions.wide.infill)
data.proportions.wide.infill <- data.proportions.wide.infill[data.proportions.wide.infill$od %in% days.missing,]
data.proportions.wide.infill$year <- 2001
data.proportions.wide.infill$prop <- data.proportions.wide.infill$year2001
#newdata$p <- rowMeans(data.proportions.wide[is.na(data.proportions.wide$year2001),covariates])

ggplot(data.proportions$data.yearly.daily, aes(od, prop))+
  geom_line()+
  geom_line(data=data.proportions.wide.infill,  col='red')+
  facet_wrap("year")

str(data.proportions$data.yearly.daily)

normal.lik2 <- function(theta,y){
  mu<-theta[1]
  sigma<-theta[2]
  n<-length(y)
  z<-(y-mu)/sigma
  logl<- -n*log(sigma) - sum(log(dnorm(z)))
  return(-logl)
}



res <- lapply(split(data.proportions$data.yearly.daily, data.proportions$data.yearly.daily$year), function(x,mean.n){

  p100 <- max(x$od[x$prop !=0])+1
  x <- x[order(x$prop),]
  mode.v <- mean(tail(x$od, mean.n))
  mode.p <- mean(tail(x$prop, mean.n))
  sd.v <- sd <- utilizer:::calcSDfromquantiles(q1 = mode.v, q2 = p100, p2 = 0.99999)
  # df <- data.frame(year=unique(x$year), mode=mode, mode.p=mode.p, p100=p100, sd=sd)

  od <- 130:270
  #mle stuff
  #para <- optim(c(174,16), normal.lik2,y=x$prop)$par
  #prop <- dnorm(od, para[1], para[2])
  #y.at.meanx <- x$prop[abs(x$od-para[1])==min(abs(x$od-para[1]))]
  #scaler <- y.at.meanx/dnorm(para[1], para[1], para[1])

  prop <- dnorm(od, mode.v, sd.v)
  scaler <- mode.p/max(prop)
  prop.f <- prop*scaler
  df <- data.frame(year=unique(x$year), od=od, prop.f=prop.f)
  #df[df$od<mode.v,]
  df
}, mean.n=5)

data.forecasts <- do.call(rbind, res)

data.forecasts <- merge(data.proportions$data.yearly.daily, data.forecasts, all = TRUE)
data.forecasts$prop.f[!is.na(data.forecasts$prop)] <- NA

dev.set(which=4)
ggplot(data.forecasts, aes(od, prop))+
  geom_line()+
  geom_line(aes(y=prop.f),  col='red')+
  facet_wrap("year")

View(data.forecasts)

data.forecasts <- lapply(split(data.forecasts, data.forecasts$year), function(x){
  if(unique(x$year) %in% c(2001:2008, 2018, 2021)){
  cpue.sum.est <- max(x$cpue.cumul,na.rm = TRUE)/(1-sum(x$prop.f, na.rm = TRUE))
  x$cpue[is.na(x$cpue)] <- cpue.sum.est*x$prop.f[is.na(x$cpue)]
  x$cpue.cumsum <- cumsum(x$cpue)}
  x
})
data.forecasts <- do.call(rbind, data.forecasts)
View(data.forecasts)




# mle approach ------------------------------------------------------------

filename <- "C:/Users/folkesm/Documents/Projects/salmon/skeena/inseason_forecasting/data/temporary/data.set.tides.fullseries.Rds"
data.set.tides <- readRDS(filename)
str(data.set.tides)
unique(data.set.tides$net_config_nme)

data.set.multi <- data.set.tides[grepl("multi", data.set.tides$net_config_nme, ignore.case = TRUE) & data.set.tides$year<2001,]
data.set.mono <- data.set.tides[grepl("mono", data.set.tides$net_config_nme, ignore.case = TRUE) & data.set.tides$year>=2001,]
#data.set <- rbind(data.set.multi, data.set.mono)
data.set <- data.set.mono
fit.res <- loess(cpue~rate.avg,  data = data.set)

data.set$cpue.pred <- as.numeric(predict(fit.res, data.set[, "rate.avg", drop=FALSE]))
data.set$cpue.pred[data.set$cpue.pred<0] <- 0

base.rate <- 0
cpue.pred.base <-  as.numeric(predict(fit.res, data.frame(rate.avg=base.rate)))

data.set$cpue.scaler <- data.set$cpue.pred/cpue.pred.base
data.set$cpue.adj <- data.set$cpue/data.set$cpue.scaler
data.set$cpue.adj[is.infinite(data.set$cpue.adj)] <- 0

data.daily <- aggregate(cpue.adj~fe_start_date+od+year, data=data.set, mean)

res <- with(data.daily, by(data.daily, list(year), function(x)data.frame(fe_start_date=x$fe_start_date, cpue.cumsum= cumsum(x$cpue.adj))))
res <- do.call(rbind, res)
data.daily <- merge(data.daily, res)

#___________

data.daily$cpue <- data.daily$cpue.adj
data.proportions <- calcDailyProportions(data.daily)
data.proportions$data.yearly.daily <- data.proportions$data.yearly.daily[data.proportions$data.yearly.daily$year !=2023,]

#of the total witnessed run, this is the lowest proportion seen
res <- do.call(rbind, lapply(split(data.proportions$data.yearly.daily, data.proportions$data.yearly.daily$year), function(x)cbind(unique(x$year), min(x$prop.cumul))))
res[order(res[,2]),]
hist(res[,2], breaks = seq(0,.03, by=.001))
quantile(res[,2])
fn <- ecdf(res[,2])
quantile(res[,2], probs = 0.95)
fn(0.0075)
fn(0.0025)
res[res[,2]>=0.0075,]

#first fishing day in each year
aggregate(od~year, data.proportions$data.yearly.daily[!is.na(data.proportions$data.yearly.daily$prop.cumul),], min)



