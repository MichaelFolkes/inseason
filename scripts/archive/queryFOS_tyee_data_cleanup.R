

# comments ----------------------------------------------------------------

#author: Michael Folkes
#date:  "2022-06-21 16:19:45 PDT"

#comment: This script imports the tyee test fish raw data tables queried from FOS. (excluding biosamples).

# this includes a few lines to query fos (if you haven't done that in the separate script)
# cleans the data (parses some fields into separate fields of numbers and text)
# imports an xls file that has fixes to the table of panel_effort times (eventually can be excluded)
# imports an xls file that allows parsing of log recorders into a new table of parsed names
# exports to a single xls with multiple sheets (one for each table).  This data includes all salmon and steelhead.

#you will need an account with FOS to query it.
# have a csv file (likely named fos_id.csv) with your username and pw that will look like this (no # symbols):
#username,password
#folkesm,mypassword



# setup -------------------------------------------------------------------

rm(list = ls())

# packages ----------------------------------------------------------------


require(openxlsx)
require(inseason)


# function defs -----------------------------------------------------------

# variables ---------------------------------------------------------------


#this is where the xls file will go.  will need to revise path to suite your drive:
path.export <- "C:/Users/folkesm/Documents/Projects/salmon/skeena/inseason_forecasting/data/fos_reports/full_extraction"


# data:query FOS ----------------------------------------------------------

#source of username and pw for FOS:
userid <- read.csv("C:/Users/folkesm/Documents/Projects/salmon/skeena/inseason_forecasting/R/extras/fos_id.csv", strip.white = TRUE)

data.fos.raw <- queryFOStyee_tables(userid = userid)

#check to see you have lots of records
lapply(data.fos.raw$data, dim)

#save raw file to an rds file
filename <- paste0(path.export, "/data.fos.raw_", Sys.Date(), ".Rds")
saveRDS(object = data.fos.raw, file = filename)



# data:import RDS of raw data ----------------------------------------------

#if you queried FOS using the separate script, then import the rds file

filepath.rds <- list.files(path = path.export, "rds$", ignore.case = TRUE, full.names = TRUE)
# have a look at what rds file you want to import
filepath.rds
data.fos.raw <- readRDS(filepath.rds[4])

str(data.fos.raw$data$panel_effort)




# data:cleanup ------------------------------------------------------------

#this is the xls file of panel time corrections:
data.panel.edits <- as.data.frame(readxl::read_excel("C:/Users/folkesm/Documents/Projects/salmon/skeena/inseason_forecasting/data/fos_reports/full_extraction/2022-04-27/TimeCorrections_May11_2022.xlsx"))

#don't need as it's now in package data
#data.recorder.translate <- read_excel("C:/Users/folkesm/Documents/Projects/salmon/skeena/inseason_forecasting/data/fos_reports/full_extraction/2022-04-27/observer_translate.xlsx")

data.fos <- cleanupFOStyee(data.fos.raw, data.recorder.translate = data.recorder.translate,  data.panel.edits=data.panel.edits)
str(data.fos$data$panel_effort)

head(data.fos.raw$data$panel_effort$NET_START_OUT)
head(data.fos$data$panel_effort$NET_START_OUT)

tail(data.fos.raw$data$panel_effort$NET_START_OUT)
tail(data.fos$data$panel_effort$NET_START_OUT)


# parse seal data from comments ----------------------------------------------
#for current year, parse seal data from comments and add to 'seals' column

View(data.fos$data$set[data.fos$data$set$YEAR==2022,])

#these data now fixed
# data.aux <- read.csv("C:/Users/folkesm/Documents/Projects/salmon/skeena/inseason_forecasting/data/fos_reports/tyee_testfish/tyee_auxiliary.txt")
# data.aux$FE_START_DATE <- as.Date(data.aux$FE_START_DATE)
# data.fos$data$set$SEALS[data.fos$data$set$FE_START_DATE %in% data.aux$FE_START_DATE & data.fos$data$set$SET_NO %in% data.aux$SET_NO] <- data.aux$seals.aux
# data.fos$data$set[data.fos$data$set$FE_START_DATE %in% data.aux$FE_START_DATE & data.fos$data$set$SET_NO %in% data.aux$SET_NO,]

data.fos$data$set <- parseSealComments(set = data.fos$data$set)




nrow(data.fos.raw$data$panel_effort)
nrow(data.fos$data$panel_effort)


# data:export to RDS ------------------------------------------------------

filepath <- paste0(path.export, "/", "data.fos.processed_", Sys.Date(), ".Rds")
saveRDS(data.fos, file = filepath)


# data:export to xlsx -------------------------------------------------------------


#need to flatten list prior to export
comments <- do.call(rbind, data.fos$comments[! names(data.fos$comments) %in% "recorders.by.year"])
comments <- paste(row.names(comments), comments, sep=": ")
comments <- as.data.frame(comments)
list.tmp <- c(comments,data.fos$data)
list.tmp$set$FE_START_DATE <- format(list.tmp$set$FE_START_DATE, "%Y-%m-%d")


#when importing with readxl, it assumes timezone is utc so explicitly add tz to value and make a character:
net_time.colnames <- names(list.tmp$panel_effort)[grep("net_start|net_full", names(list.tmp$panel_effort), ignore.case = TRUE)]
net_time.colnames

for (column in net_time.colnames) {
  list.tmp$panel_effort[, column] <- format(list.tmp$panel_effort[, column], format = "%Y-%m-%d %H:%M:%S", usetz=TRUE)
}

filepath <- paste0(path.export, "/", "data.fos.processed_", Sys.Date(), ".xlsx")
openxlsx::write.xlsx(list.tmp, file=filepath, rownames=FALSE)


